# Subquest List

# Requirements
- [Markdown Checkbox](https://marketplace.visualstudio.com/items?itemName=PKief.markdown-checkbox)
    - Keybindings
        - `Ctrl` + `Shift` + `c`: create
        - `Ctrl` + `Shift` + `Enter`: toggle
    - Settings
        - Date When Checked: off
        - Italic When Checked: off