import argparse
import datetime
import pathlib
import shutil


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        "generate_daily_file.py",
        description="Generate daily file into subquest-record.",
    )

    parser.add_argument(
        "-d",
        "--date",
        default=None,
        help="date for generated file name. ex) {date}.md",
    )

    args = parser.parse_args()

    return args


def generate_daily_file(date) -> None:
    output_dir = pathlib.Path("subquest-record")

    if date is None:
        date = datetime.date.today()
        date = date.strftime("%Y%m%d")

    file_path = output_dir.joinpath(f"{str(date)}.md")
    assert not file_path.exists(), f"{file_path} is existed."

    shutil.copy2(output_dir.joinpath("template.md"), file_path)


if __name__ == "__main__":
    args = parse_args()

    generate_daily_file(date=args.date)
